#include <iostream>
#include <vector>
#include <string>
#define NUMPATTERNS 4

using namespace std;

void permutate(string curStr, vector<string>& strList)
{
    int length = curStr.length();
    if (length == 1)
    {
        strList.push_back(curStr);
        return;
    }

    for (int i = 0; i < length; i++)
    {
        vector<string> curStrList;
        string newStr = curStr;
        // Erase the character at location i
        char charPos = newStr.at(i);
        newStr.erase(i, 1);
        permutate(newStr, curStrList);
        for (int j = 0; j < curStrList.size(); j++)
        {
            string& genStr = curStrList.at(j);
            string newPermutation = charPos + genStr;
            strList.push_back(newPermutation);
        }
    }
}

int main()
{
    const char* inputs[] = {"0", "01", "012", "0123", "01234", "012345", "0123456", "01234567", "012345678"};
    int sizes[NUMPATTERNS];
    for (int j = 0; j < NUMPATTERNS; j++)
    {
        string input = inputs[j]; 
        vector<string> permuatations;
        permutate(input, permuatations);
        int numPermutations = permuatations.size();
        cout << "const char* permutations_" << j + 1 << "[ ] = {";
        for (int i = 0; i < numPermutations - 1; i++)
            cout << "\"" << permuatations.at(i) << "\",";
        cout << "\"" << permuatations.at(numPermutations - 1) << "\"};" << endl;
        sizes[j] = numPermutations;
    }

    cout << "int arraySizes[] = {";
    for (int j = 0; j < NUMPATTERNS - 1; j++)
        cout << sizes[j] << ", ";
    cout << sizes[NUMPATTERNS - 1] << "};" << endl;

    cout << "const char** listOfPermutations[] = {";
    for (int i = 1; i < NUMPATTERNS; i++)
        cout << "permutations_" << i << ", ";
    cout << "permutations_" << NUMPATTERNS << "};" << endl;
}
