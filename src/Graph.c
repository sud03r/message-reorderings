#include <stdlib.h>
#include <stdio.h>
#include "Graph.h"

// Function Implementations
Node* GetNode(Graph* _self, int nodeId)
{
    if (nodeId >= _self->numNodes)
        return NULL;
    return _self->nodes[nodeId];
}

void CreateEdge(Node* src, Node* dst)
{
    Edge* edge = NEW(Edge);
    edge->src = src;
    edge->dst = dst;
    src->outEdges[src->numOutEdges++] = edge;
    dst->inEdges[dst->numInEdges++] = edge;
}

Node* CreateNode(Graph* _self, int nodeId)
{
    Node* node = NEW(Node);
    node->nodeId = nodeId;
    node->numInEdges = 0;
    node->numOutEdges = 0;
    node->depth = -1; // Set during traversal.
    _self->nodes[_self->numNodes++] = node;
}

void Pr(Graph* _self)
{
    printf("\nCreated Graph:\n");
    int i = 0;
    for (i = 0; i < _self->numNodes; i++)
    {
        Node* node = _self->nodes[i];
        printf("Node: %d (Depth: %d):\n", node->nodeId, node->depth);
        printf("Num OutEdges : %d\n", node->numOutEdges);
        int j = 0;
        for (j = 0; j < node->numOutEdges; j++)
        {
            Edge* edge = node->outEdges[j];
            printf("%d -> %d\n", edge->src->nodeId, edge->dst->nodeId);
        }
        printf("Num InEdges : %d\n", node->numInEdges);
        for (j = 0; j < node->numInEdges; j++)
        {
            Edge* edge = node->inEdges[j];
            printf("%d -> %d\n", edge->src->nodeId, edge->dst->nodeId);
        }
        printf("\n");
    }
}

Graph* CreateGraph()
{
    Graph* graph = NEW(Graph);
    graph->numNodes = 0;
    return graph;
}


Graph* CreateGraphFromAdjMatrix(int adjMatrix[][MAXNODES], int n)
{
    int i, j;
    Graph* g = CreateGraph();
    for (i = 0; i < n; i++)
        CreateNode(g, i);

    for (i = 0; i < n; i++)
    {
        Node* src = GetNode(g, i);
        for (j = 0; j < n; j++)
        {
            if (adjMatrix[i][j] == 1)
            {
                Node* dst = GetNode(g, j);
                CreateEdge(src, dst);
            }
        }
    }
    return g;
}


void doDFS(Graph* g, Node* curNode)
{
    int i;
    for (i = 0; i < curNode->numOutEdges; i++)
    {
        Edge* edge = curNode->outEdges[i];
        Node* dst = edge->dst;
        if (dst->depth == -1)
        { 
            gDebugMode && printf("{%d->%d} ", curNode->nodeId, dst->nodeId);
            dst->depth = curNode->depth + 1;
            doDFS(g, dst);
        }
        else if (dst->depth < curNode->depth + 1)
        {
            dst->depth = curNode->depth + 1;
        }
    }
}

int PopulateDepthWiseNodeInfo(Graph* g, NodeList* nList[])
{
    int i, maxDepth = 0;
    for (i = 0; i < g->numNodes; i++)
    {
        Node* node = g->nodes[i];
        NodeList* pList = nList[node->depth];
        if (pList == NULL)
        {
            pList = malloc(sizeof(NodeList));
            pList->idx = 0;
            nList[node->depth] = pList;
            if (maxDepth < node->depth)
                maxDepth = node->depth;
        }
        pList->nodes[pList->idx++] = node;
    }
    return maxDepth;
}

int GetMaximumDegree(Graph* g)
{
    int i, maxDegree = 0;
    for (i = 0; i < g->numNodes; i++)
    {
        Node* node = g->nodes[i];
        if (maxDegree < node->numInEdges)
            maxDegree = node->numInEdges;
    }
    return maxDegree;
}
