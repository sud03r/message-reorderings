#include <stdio.h>
#include "Graph.h"
#define MAXDEPTH 10

int gDebugMode = 0;

// APIs for getting permutations of an array.
int getNumPermutations(int size);
int shuffleArray(void* origArr[], void* newArr[], int num, int permIdx);

void PrintLevelWiseInfo(NodeList* nList[], int maxDepth)
{
    // Print level wise info
    int i;
    for (i = 0; i <= maxDepth; i++)
    {
        printf("Depth %d : ", i);
        int j;
        for (j = 0; j < nList[i]->idx; j++)
        {
            printf("%d ", nList[i]->nodes[j]->nodeId);
        }
        printf("\n");
    }
}

void PrintIncomingEdges(Node* node, int permIdx)
{
    int i;
    void* origArr[MAXNODES];
    void* shuffledArr[MAXNODES];
    for (i = 0; i < node->numInEdges; i++)
        origArr[i] = (void*)node->inEdges[i];

    int valid = shuffleArray(origArr, shuffledArr, node->numInEdges, permIdx);
    for (i = 0; i < node->numInEdges; i++)
    {
        Edge* curEdge = NULL;
        if (valid)
            curEdge = (Edge*)shuffledArr[i];
        else
            curEdge = node->inEdges[i];
        printf("(%d -> %d) \n", curEdge->src->nodeId, curEdge->dst->nodeId);
    }
}

void GeneratePermuations(Graph* g, NodeList* nList[], int maxDepth)
{
    int i, permIdx = 0;
    int maxDegree = GetMaximumDegree(g);
    int maxPermIdx = getNumPermutations(maxDegree);
    
    for (permIdx = 0; permIdx < maxDegree; permIdx++)
    {
        printf("Arrangement #%d:\n\n", permIdx);
        for (i = 0; i <= maxDepth; i++)
        {
            int j;
            for (j = 0; j < nList[i]->idx; j++)
            {
                Node* node = nList[i]->nodes[j];
                PrintIncomingEdges(node, permIdx);
            }
        }
    }
}

int main()
{
    int adjMatrix[MAXNODES][MAXNODES] = {{0, 1, 1, 0}, {0, 0, 1, 0}, {0, 0, 0, 1}, {0, 0, 0, 0}};
    // Create Graph
    Graph* g = CreateGraphFromAdjMatrix(adjMatrix, 4);

    // Do DFS and assign a levelId to each node.
    Node* start = GetNode(g, 0);
    start->depth = 0;
    //printf("DFS traversal sequence on graph:\n");
    doDFS(g, start);
    
    // Print Created Graph
    if (gDebugMode)
        Pr(g);
    
    // Create NodeList for nodes at each level
    NodeList* nList[MAXDEPTH] = {0};
    int maxDepth = PopulateDepthWiseNodeInfo(g, nList);

    if (gDebugMode)
        PrintLevelWiseInfo(nList, maxDepth);
   
    GeneratePermuations(g, nList, maxDepth);

    return 0;
}


