#include <stdio.h>
#include <assert.h>
#include "Permutations.h"
#define MAX_ENTRIES 10

int getNumPermutations(int size)
{
    return arraySizes[size - 1];
}

int shuffleArray(void* origArr[], void* newArr[], int num, int permIdx)
{
    if (permIdx >= getNumPermutations(num))
        return 0;

    assert(permIdx < getNumPermutations(num));
    const char** pListPtr = listOfPermutations[num - 1];
    const char* permutation = pListPtr[permIdx];
    const char* cptr = permutation;
    int oldIdx = 0;
    while (*cptr)
    {
        char val = *cptr;
        int newIdx = val - '0';
        newArr[newIdx] = origArr[oldIdx];
        cptr++;
        oldIdx++;
    }
    return 1;
}

// For unit testing
/*int main()
{
    void* origArr[] = {(void*)0x1, (void*)0x2, (void*)0x3};
    void* newArr[] = {0, 0, 0};
    shuffleArray(origArr, newArr, 3, 4);
    
    printf("Original\n");
    int i;
    for (i = 0; i < 3; i++)
        printf("%p ", origArr[i]);
    printf("\n");
    
    printf("Shuffled\n");
    for (i = 0; i < 3; i++)
        printf("%p ", newArr[i]);
    printf("\n");
}*/
