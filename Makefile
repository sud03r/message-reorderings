
GEN_DIR = ./generate
SRC_DIR = ./src
BIN_DIR = ./bin
INCLUDE_DIR = ./include
CC = gcc -I$(INCLUDE_DIR)
OBJECTS := $(subst src,bin,$(patsubst %.c,%.o,$(wildcard $(SRC_DIR)/*.c)))
INCLUDES := $(wildcard $(INCLUDE_DIR)/*)
all: genReorders

$(INCLUDE_DIR)/Permutations.h: $(GEN_DIR)/permutations.cc
	g++ $(GEN_DIR)/permutations.cc
	@./a.out > $(INCLUDE_DIR)/Permutations.h
	@\rm a.out

$(BIN_DIR)/%.o : $(SRC_DIR)/%.c $(INCLUDES)
	$(CC) -c -g -o $@ $<

genReorders: $(OBJECTS)
	$(CC) $^ -o $@

run : genReorders
	./genReorders

clean:
	\rm -f genReorders bin/*

.PHONY: clean run
