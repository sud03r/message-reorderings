struct _node {
    void* data;
    struct _node* next;
};
typedef struct _node Node;

typedef struct {
    Node* head;
    Node* tail;
    int size;
} Queue;

int IsEmpty(Queue* q)
{
    return (q->size == 0);
}

void InitQueue(Queue* q)
{
    // create a dummy node
    Node* node = malloc(sizeof(Node));
    node->data = NULL;
    node->next = NULL;
    q->head = node;
    q->tail = node;
    q->size = 0;
}

void Insert(Queue* q, void* el)
{
    // Insertion at end
    Node* node = malloc(sizeof(Node));
    node->data = el;
    node->next = NULL;
    q->tail->next = node;
    q->tail = node;
    q->size++;
}

void* Remove(Queue* q)
{
    // Remove front node
    if (q->size > 0)
    {
        Node* front = q->head->next;
        q->head->next = front->next;
        q->size--;
        return front->data;
    }
    else
        return NULL;
}

// For Example Usage and Unit testing
/*
int main()
{
    Queue q;
    InitQueue(&q);
    printf("Before, Empty : %d\n", IsEmpty(&q));
    Insert(&q, (void*)1);
    Insert(&q, (void*)2);
    Insert(&q, (void*)3);
    printf("After, Empty : %d\n", IsEmpty(&q));
    printf("First: %p\n", Remove(&q));
    printf("Second: %p\n", Remove(&q));
    printf("Last: %p\n", Remove(&q));
    printf("Should be NULL: %p\n", Remove(&q));
    printf("Finally, Empty : %d\n", IsEmpty(&q));
}
*/
