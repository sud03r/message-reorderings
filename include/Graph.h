#define MAXNODES 100
#define NEW(object) malloc(sizeof(object))

extern int gDebugMode;

struct _Node;

struct _Edge {
    struct _Node* src;
    struct _Node* dst;
};

struct _Node {
    int nodeId;
    int depth;
    int numInEdges;
    int numOutEdges;
    struct _Edge* outEdges[MAXNODES];
    struct _Edge* inEdges[MAXNODES];
};

struct _Graph {
    int numNodes;
    struct _Node* nodes[MAXNODES];
};

typedef struct _Node Node;
typedef struct _Edge Edge;
typedef struct _Graph Graph;

typedef struct {
    Node* nodes[MAXNODES];
    int idx;
} NodeList;


// Function Declarations
Node* GetNode(Graph* _self, int nodeId);
void CreateEdge(Node* src, Node* dst);
Node* CreateNode(Graph* _self, int nodeId);
void Pr(Graph* _self);
Graph* CreateGraph();
Graph* CreateGraphFromAdjMatrix(int adjMatrix[][MAXNODES], int n);
void doDFS(Graph* g, Node* curNode);
int PopulateDepthWiseNodeInfo(Graph* g, NodeList* nList[]);
int GetMaximumDegree(Graph* g);

// TODO: Cleanup!
