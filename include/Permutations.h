const char* permutations_1[ ] = {"0"};
const char* permutations_2[ ] = {"01","10"};
const char* permutations_3[ ] = {"012","021","102","120","201","210"};
const char* permutations_4[ ] = {"0123","0132","0213","0231","0312","0321","1023","1032","1203","1230","1302","1320","2013","2031","2103","2130","2301","2310","3012","3021","3102","3120","3201","3210"};
int arraySizes[] = {1, 2, 6, 24};
const char** listOfPermutations[] = {permutations_1, permutations_2, permutations_3, permutations_4};
